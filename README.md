# 2048 小游戏- OpenHarmony版

#### 介绍

2048游戏的规则是这样的：在一个4乘以4的格子里，游戏开始后，从所有的空格子中随机选择两个空格子，对于随机选择的这两个空格子，在每个空格子里填入2或4。接下来，使用上下左右四个方向键进行操作。当按某个方向键时，所有非空的格子都会往这个方向滑动。如果在滑动的过程中两个数字相同的格子碰到一起，那么就会把这两个数字相加从而合并成一个新格子。只要有格子合并，分数就会增加。滑动并合并之后，会从所有的空格子中随机选择一个空格子，在其中填入2或4。游戏的目标是通过不停地滑动格子和合并格子，在所有格子中拼出一个2048。如果格子全满了，并且，不管向哪个方向滑动格子都不会合并，那么游戏就结束了。整个游戏的规则非常简单，老少皆宜，但是要想拼出一个2048来，并不是那么容易的事情。


#### 实现思路

向左滑动的图示：
![输入图片说明](https://images.gitee.com/uploads/images/2022/0418/191328_195df1dc_8122337.png "屏幕截图.png")     

向右滑动的图示：
![输入图片说明](https://images.gitee.com/uploads/images/2022/0418/191501_cad718bb_8122337.png "屏幕截图.png")

#### 硬件

[润和大禹200 RK3568开发板](https://item.taobao.com/item.htm?spm=a2126o.success.result.1.44984831HpwtGV&id=655971020101)     

#### 系统版本

version-Master_Version-OpenHarmony_3.1.5.1-20220221_201100-dayu200.tar.gz     
 

#### 环境变量

```sh
export NPM_NODE_HOME=/home/margaret-lau/Documents/node-v12.16.1-linux-x64
export JAVA_HOME=/home/margaret-lau/Documents/jdk-11.0.6
export GRADLE_USER_HOME=/home/margaret-lau/.gradle    
export OHOS_SDK_HOME=/home/margaret-lau/SDK    
export PATH=$JAVA_HOME/bin:$NPM_NODE_HOME/bin:$PATH
```

#### 编译命令

1. 生成hap包：`npm run debug:hap` 
2. 将hap包推送到开发板上：`hdc_std file send xxx.hap /data/local/tmp/xxx.hap`
3. 修改hap包权限：`chmod 777 /data/local/tmp/xxx.hap`
4. 安装hap包：`bm install -rp /data/local/tmp/xxx.hap`

